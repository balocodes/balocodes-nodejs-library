"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Created by @balocodes
const redis = __importStar(require("redis"));
class RedisCtrl {
    constructor() {
        this.activeClient = this.makeClient();
    }
    makeClient() {
        return redis.createClient();
    }
    watchConnection() {
        this.activeClient.on("connect", function () {
            console.log("Redis client connected");
        });
        this.activeClient.on('error', function (err) {
            console.log('Something went wrong ' + err);
        });
    }
    set(data) {
        this.activeClient.set(data.key, data.value, redis.print);
    }
    get(data) {
        this.activeClient.get(data.key, data.func);
    }
}
exports.redisCtrl = new RedisCtrl();
//# sourceMappingURL=redisCtrl.js.map