"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// Created by @balocodes
const mongoose_1 = require("mongoose");
const mssql = require("mssql");
exports.mongooseFunc = function mongooseConnect(link) {
    return __awaiter(this, void 0, void 0, function* () {
        let uri = link || "mongodb://127.0.0.1:27017/fresh";
        if (!link && process.env.ATLAS_SMART_CLINIC) {
            uri = process.env.ATLAS_SMART_CLINIC;
        }
        if (process.env.REPLICA_SET) {
            uri = process.env.REPLICA_SET;
        }
        else if (process.env.NODE_ENV === "test") {
            uri = "mongodb://127.0.0.1:27017/fresh-test";
        }
        else if (process.env.NODE_ENV === "full_test") {
            uri = "mongodb://127.0.0.1:27017/full-fresh-test";
        }
        console.log(uri);
        mongoose_1.connect(uri, { useFindAndModify: false, useCreateIndex: true, useNewUrlParser: true }).then((resp) => __awaiter(this, void 0, void 0, function* () {
            resp.connection.on("error", console.error.bind(console, "MongoDB ERROR"));
            resp.connection.on("connect", () => { console.log("DB connection successful"); });
            if (process.env.NODE_ENV === "full_test") {
                yield resp.connection.dropDatabase();
            }
            console.log("Models", resp.connection.modelNames());
        })).catch((err) => {
            console.log("Could not connect to DB", err);
        });
    });
};
exports.mssqlFunc = function mssqlConnect(config) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield mssql.connect(config);
            const result = yield mssql.query `select * from mytable where id != 0`;
            console.dir(result);
        }
        catch (err) {
            console.log(err);
        }
    });
};
//# sourceMappingURL=connect.js.map