import { ClientSession, DocumentQuery, Document } from "mongoose";
export declare class TransactionChainer {
    chainedTransactions: DocumentQuery<Document | null, Document, {}>[];
    isFakeSession: boolean;
    constructor(chainedTransactions: DocumentQuery<Document | null, Document, {}>[]);
    /**
     * Determine whether or not to start an active session and starts session if required
     * Checks if process.env.RUNNING_REPLICA is set to "true"
     */
    startSessionMethod: () => Promise<ClientSession | null>;
    /**
     * Runs each query using the same transaction context
     */
    transactionsRunner: () => Promise<boolean>;
}
